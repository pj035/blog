---
layout: post
title: School Project to build a Covid-19 App
---

Recently, there was this idea in my company and among its friends to build a simple mobile application to indicate
Covid-19 cases per 100k inhabitants. It should be visible as a traffic light metaphor. One of the company's friends asked us how much time we would need to build an app like this.

## An Idea was born

While thinking about this back then in October(?) I realized that this mobile app is quite a good use case for some kind of intern or a school project. Luckily we have contact to a teacher from a local highschool, who teaches a "digitalization" course this year. After a few calls we had a rough plan how to approach this. I will take care of software development and a colleague will teach the pupils UI/UX and how to generally approach an app's user base.

Due to a lot of constraints the course is quite a challenge. To name a few:
* Really bad technical conditions in German schools - if WiFi is available, you can consider it a modern school..
* We can't expect any previous knowledge in software development.
* Keep the fun in first place! They should learn and have fun, so we need fast and quite easily achievable results.
* They only work on this once a week for at most 3h. I.e. we need small enough work packages.

While writing the guide for the first lecture I noticed how much I underestimated the second point. All the tools, commands and environments that are part of my everyday (work) life are unknown to them (likely). So I had to start from scratch: how to open a shell, HTML, TS files, open the dev-tools in your browser, install npm deps and start the build process. Then I went on with some basic stuff like this is a button, here is a variable. If we do this, then we assign some value and with magic the value is displayed in the HTML.

Creating such a fine-grained tutorial was really a challenge. You don't want to spoil them too much, they should do a few things on their own - but which with such conditions?

## The first Day

Today, 18th of Novemeber 2020, was the first day of the project and oh boy.. what a mess! :-) Obviously some of the technical parts failed.
We had to solve a lot of communication issues related to the bad ISP and internet condition in Germany.
We forgot to install and setup some of the required software on the test notebooks, but in the end we finally made it and I had the impression that everyone was quite happy with the new experience.

Since I was responsible for the developer teams I briefly want to summarize what we achieved today. First of all we looked into the command line to install npm modules and starting the Angular build process (`ng serve`). Once that was done a few basic elements of the HTML file were changed. We added a button and upon click event we loaded a Covid-19 map from a server and displayed that in the app. The result looks like this:

![App after first lecture]({{ site.url }}/assets/images/2020-11/covid-19-app-1.gif)


Next time we will fetch data from a server, telling us how many people are infected per 100k inhabitants. I have to prepare that still tho and especially consider my learnings of today's lecture.

## Miscs

I'll try to provide an update every week. The project runs until mid of December.

And maybe I'll publish my guide here. Even tho it's written in German, it might be a good idea. 🤔