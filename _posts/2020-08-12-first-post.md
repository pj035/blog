---
layout: post
title: Let's see what this is all about
---

So.. my first blog post ever. At least I think so. Actually I'm still not sure what to write, but my current idea is to fill this blog with my everyday's life problems and their (hopefully good) solutions - mainly about tech things. It could serve as my private backup for guides.

Do I have to introduce myself? Well, I'm Philipp, a <s>computer science</s> tech-enthusiast with a general interest in this fascinating thing called life. I'm open and direct - if you have any questions write me. At least if I put some contact details on this page.
Usually I like to solve problems in an analytical way, but one of my biggest flaws is kinda tied to that: I'm often torn away by other, new things that occupy more attention suddenly. At time of writing I'm employeed by a small company as a technical lead, developing indiviual software solutions teogether with a remote team (sitting mainly in East Africa).

To cover some of my current interests:
* 3D Printing (FDM) - I have one of those cheap chinese DIY kits at home, but after a ton of upgrades and learnings it produces decent results. It was and is a nice tinkering journey.
* Bikes - I really love to ride bikes as well as tinker them<sup>1</sup>. 
* Work out - I'm doing this for quite some years now. More recently I integrated Yoga into my routine as an addition to the heavy lifting.
* Gaming - at least every now and then. ¯\\\_(ツ)_/¯
* Last, but most important: a general interest on almost anything. I love to learn new things and I'm curios how the world works.

<sup>1</sup>Yeah, I love to tinker around...

Doesn't sound so techy at all after writing it. Anyhow, let's see where this journey will take us (mostly me).


Disclaimer: Don't expect frequent updates.