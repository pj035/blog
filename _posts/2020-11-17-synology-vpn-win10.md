---
layout: post
title: Synology IP-Sec VPN for Windows 10
---

Wow, long time no see. My last post was quite some time ago.. Anyhow, I have a brief documentation for my local Synology VPN and Windows 10.

## Basic Setup

Today I want to document how I set up a VPN connection to my Synology's VPN server on Windows 10. As a good source I recommend [Christian's blog](https://christian-brauweiler.de/vpn-l2tp-ipsec-mit-synology-diskstation-und-windows-10/). He covers all steps for the basic setup, but I had to apply two more steps - likely due to my local pihole setup.

First setup the IPSec/L2tp VPN server on your Synology. Follow Christian's instructions or look directly at the Synology docs. After that part is done we have to configure Windows 10 according to Christian's blog. That means we have to setup an IPSec with preshared key VPN connection and afterwards "hack" the registry:

1. Navigate to `HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\PolicyAgent`
2. Create a new DWORD-32-Bit with name `AssumeUDPEncapsulationContextOnSendRule`
3. Set value to `2` (hex)

Afterwards, a few services need to be checked and maybe changed. Type `services` in the Windows searchbar and open the corresponding tool. Search for `IPsec-Richtlinien-Agent` and `IKE- und AuthIP IPsec-Schlüsselerstellungsmodule` and set the starttype for both to `Automatic`.

## Solving my local DNS Problem

Once that was set up I could connect to my local network via VPN. This was working fine. My usual test setup consists of a mobile hotspot on my phone using mobile network and connecting the Windows machine to that hotspot. Pinging my home network's devices worked flawlessly - as long as I was using their IP. But since IP's are not so easy to remember or type I rather want to rely on that thing called DNS. E.g. my nas' name on the network is `nas` (creative enough?).

On my home network I have a few devices - most of interest are my Synology NAS and a Pi (1) running Pihole. The Pihole acts as a local DNS and unfortunately my Windows machine couldn't resolve the names it propagated. I could see that Windows was using the Pi's IP as DNS (check `ipconfig /all` in cmd), but it still didn't work. 

As a side note, since my Pi acts a local DNS I have configured the domains (e.g. `nas`) in the locale DNS records of Pihole.

## The Solution...

...(for my setup) was to change the the VPN connection's settings from being a **public to a private** network profile. Idk, who set it public in first place - me?!
Anyhow, to do that, go to your Window's settings and type VPN in the searchbar. Open the menu and click on your VPN connection - establish a connection. Once that is done, click on advanced/further settings and change your profile to a private network.