---
layout: post
title: Mobile App Development with Flutter - an IMHO
---

A brief IMHO on Flutter.<br>
*PS: It's quite nice!*

## What is Flutter?

For those who never heard of Flutter, let me give you a short introduction. Flutter is a framework to build mobile apps. It's built by Google and it's advertised on its <a target="_blank" href="https://flutter.dev/">webpage</a> as

> Flutter is Google’s UI toolkit for building beautiful, natively compiled applications for mobile, web, and desktop from a single codebase.

Flutter, which is based upon <a target="blank" href="https://dart.dev/">Dart programming language</a>, seems to be quite new on the table. There are older competitors for mobile and hybrid<sup>1</sup> app development. To name a few, there is 
<a href="https://dotnet.microsoft.com/apps/xamarin" target="_blank">Xamarin</a>, <a href="https://ionicframework.com/" target="blank">Ionic</a>, <a href="https://reactnative.dev/" target="blank">React Native</a>, <a href="https://nativescript.org/" target="blank">NativeScript</a> and probably a more. 
According to <a href="https://trends.google.com/trends/explore?date=today%205-y&q=%2Fg%2F11f03_rzbg,%2Fg%2F1q6l_n0n0,%2Fg%2F11h03gfxy9" target="blank">Google Trends</a> Flutter became quite popular in the past years.

<script type="text/javascript" src="https://ssl.gstatic.com/trends_nrtr/2431_RC04/embed_loader.js"></script> <script type="text/javascript"> trends.embed.renderExploreWidget("TIMESERIES", {"comparisonItem":[{"keyword":"/g/11f03_rzbg","geo":"","time":"2016-03-23 2021-03-23"},{"keyword":"/g/1q6l_n0n0","geo":"","time":"2016-03-23 2021-03-23"},{"keyword":"/g/11h03gfxy9","geo":"","time":"2016-03-23 2021-03-23"}],"category":0,"property":""}, {"exploreQuery":"date=today%205-y&q=%2Fg%2F11f03_rzbg,%2Fg%2F1q6l_n0n0,%2Fg%2F11h03gfxy9","guestPath":"https://trends.google.com:443/trends/embed/"}); </script>

<sup>1</sup>That usually means writing one codebase for multiple (mobile) plattforms.

## My Journey to Flutter

So what's my part in all of this? Well nothing spectacular (yet 😅), but I started to work on some small Flutter apps in my free time. A lot of people (Twitter bubble) as well as some local companies are talking (or working with) Flutter and I'm curious. So I started out and in this part I'd like to write about my personal experience with Flutter so far.<br>
As background it might worth to note that in my professional career I'm part of mobile app development with Ionic for a few years now.

As a first step, like with every other new language or tool, one needs to install, setup everything and follow the first tutorials. I won't go into details here nor rephrase what one can find on the Flutter website, but let me tell you that all of this works like a charm. Within an evening one is able to have the first small example app running on their device. 

Soon I started to work on a few first ideas to learn about all the available widgets and plugins, but I abandon most of them after another one came to my mind - pretty common I guess. Currently I'm working on a small app that display's <a href="https://apod.nasa.gov/apod/astropix.html" target="blank">NASA's astronomy pictures of the day</a> (APoD) in a more modern style than the website. Yes, I'm aware that there are like a million available apps doing this, but I wanted to learn more about Flutter and implementing this was quite a good learning. I didn't had to think about writing a custom backend or similar and could focus solely on the app.

Speaking of my APoD app: here is a <a href="https://play.google.com/store/apps/details?id=de.pjaecks.nasaapod" target="blank">Google Play Store link</a>. You can find the app's source code on <a href="https://gitlab.com/pj035/flutter-nasa-apod" target="blank">my Gitlab account</a>. Feel free to have a look and leave feedback. 

<center>
<video src="{{ site.baseurl }}/assets/video/flutter-apod.mp4" controls="true"></video> 
</center>

The journey to that app was delightful. I've seen first views quite fast, then I was torn away by some animation stuff and CI/CD. I came back to write some tests and refactored state management using the <a href="https://pub.dev/packages/provider" target="_blank">Provider</a> package. All of this without major issues, well there was a bit over-engineering here and there, but that's not Flutter related. 😉

Let me highlight one outstanding thing: the proper tooling around the framework. Yes, the tooling. Coming from the Node.js and NPM world I think that Flutter's way of doing things helps a lot to start fast and takes away a lot of configuration and burden from the developer. One doesn't need to care about code formatting (built-in) or how to setup the test environment.
All of this works out of the box.


### A few Issues I've faced

Even tho I had a lot of good experiences so far, I faced a few issues along the road. I want to point out two of them. In defense of Flutter and its community: both topics don't seem to be a big deal right now.

My first idea was using <a href="https://pub.dev/packages/camera" target="blank">camera plugin</a> and back then its documentation and published version were not matching (around version 0.6). Imagine me, as a Flutter newbie, trying out the first package from pub.dev and then it's not working. The error messages were not really helpful. But there was already an existing issue on github and it was resolved fast - even tho it was during Christmas.

Another topic is the package registry <a href="https://pub.dev" target="_blank">pub.dev</a>. There are a lot of good packages and one can find a solution for almost any problem. But on the other hand it seems that quite a few packages are not maintained very well - especially if not part of the official Flutter packages. I assume the reason for this is Flutter's and Dart's young age. Maybe I'm a bit spoiled by having better experiences in the NPM world here (even tho this problem exists there too). Anyhow, I'm confident that this will change!

Tbh, not so bad at all.

## IMHO

Honestly I see a lot of positive things in Flutter. Most of their claims (smooth performance, fast development...) seem to be true - so far. Building my app above was quite a pleasure and I was doing good progress most of the time. Next step is to work further on my app and then start to work on a bigger one. Maybe there will be an opportunity to do this at work. Who knows?